var express = require("express");
var router = express.Router();
var rp = require("request-promise");

var options = {
  uri: "https://jobs.github.com/positions.json?location=new+york",
  json: true // Automatically parses the JSON string in the response
};

router.get("/", function(req, res, next) {
  rp(options)
    .then(function(jobs) {
      console.log("User has %d jobs", jobs.length);
      res.render("index", {
        title: "Code on the Spot",
        jobs: jobs
      });
    })
    .catch(function(err) {
      res.render("index", { title: "Error" });
      // API call failed...
    }); /* GET home page. */
});

router.get("/servertime", function(req, res, next) {
  var first = req.query.first;
  var second = req.query.second;
  var datetime = new Date();
  var date;
  var time;
  if(first == "tanggal"){
    date = datetime.toLocaleTimeString('id');
  }
  if(second == "waktu"){
    time = datetime.toLocaleDateString(['ban', 'id']);
  }
  res.send({date , time})
});

module.exports = router;
